/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
package edu.semeru.android.mutation.major;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author KevinMoran
 *
 * This class is responsible for two major tasks 1) Using the Major mutation framework in 
 * order to generate mutant Java Files for an Android project, and 2) assembling each mutated
 * Java file into a separate mutant project for compilation.  The compilation of the mutants
 * happens outside of this class using the MutantCompiler Class.
 *
 *
 */

public class MajorMutationWrapper {

	
	/*******************************************************************
	 * @author KevinMoran
	 *
	 *This is a controller method for generating mutant Android projects.
	 *
	 *
	 *******************************************************************/
	public static void main(String[] args) throws IOException {
		

		String androidSdkFolder = "/Applications/AndroidSDK/sdk";
		String majorPath = "libs/major";
		String mutantsFiles = "/Volumes/Kevin_Data_Drive/TAMARIN_Files/mutant_files_new/";
		String mutantsPath = "/Volumes/Kevin_Data_Drive/TAMARIN_Files/mutant_src_new/";
		
		//Store Application information in Arrays for push-to-generate mutants for several projects without developer intervention.
		
//		String [] projectNameArray = {"GNU-Cash", "Mileage", "Car_Report", "Tasks", "My_Expenses"};
//		String [] projectFolderArray = {"/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/GNU_Cash", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Mileage", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Car_Report", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Tasks", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/MyExpenses"};
//		String [] srcFolderArray = {"/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/GNU_Cash/src", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Mileage/src", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Car_Report/src", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/Tasks/src", "/Users/kevinmoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/TAMARIN/instrumented_src/MyExpenses/src"};
		
		String projectName ="Car-Report";
//		String buildLogsPath = "/Users/KevinMoran/Desktop/mutant_compilation_logs";
		String mutantLogsPath = "/Volumes/Macintosh_HD_2/Research-Files/MPlus.project/MPlus-Statistics-Generation/MPlus-Mutant-Generation-Projects/mutant-logs/";
		String failedMutantsPath = "/Volumes/Macintosh_HD_2/Research-Files/MPlus.project/MPlus-Statistics-Generation/Failed-Mutants-File.txt";
		String masterMutantLog = "/Users/KevinMoran/Dropbox/Documents/My_Graduate_School_Work/SEMERU/src_code/Android_Testing/Android_Mutation_Testing/mutants.log";
		//CalcualteMutantStats(buildLogsPath, mutantLogsPath, projectName);
		//CountMutantTypes(buildLogsPath, mutantLogsPath, projectName);
		String srcFolder ="/Volumes/Kevin_Data_Drive/TAMARIN_Files/Original_Src_Files/New_Emma_Method/Car_Report/src/";
		String projectFolder = "/Volumes/Kevin_Data_Drive/TAMARIN_Files/Original_Src_Files/New_Emma_Method/Car_Report/";
		
		//countMutantTypes(args[0]);
		//generateMutantCompilationStats(args[0], args[1]);
		//countMutantTypesPerClass(args[0]);
		//String mutant_Files = generateMutantFiles(projectFolder, androidSdkFolder, majorPath, mutantsFiles, projectName, srcFolder, mutantLogsPath, masterMutantLog);
		//FixLogFiles(projectName, mutantLogsPath);
		//generateMutantProjects(projectFolder, projectName, mutantsPath, mutant_Files);
		//generateMutantCompilationStatsMPlus(mutantLogsPath, failedMutantsPath);
		
		//The loop below loops through the projects in the String Arrays above to generate the mutants for each specified project.
		
		/*int i = 0;
		
		for (String projectName : projectNameArray){
		
			System.out.println("Generating Mutants for " + projectName + " at folder: " + projectFolderArray[i]);
			
		String mutant_Files = generateMutantFiles(projectFolderArray[i], androidSdkFolder, majorPath, mutantsFiles, projectName, srcFolderArray[i], mutantLogsPath, masterMutantLog);
		
		FixLogFiles(projectName, mutantLogsPath);
		
		generateMutantProjects(projectFolderArray[i], androidSdkFolder, majorPath, mutantsFiles, projectName, mutantsPath, mutant_Files);
		
		i++;
		
		System.out.println();
		
		}*/
		
		//FixLogFiles(projectName);
	}
	 	
	/**************************************************
	 * @author KevinMoran
	 *
	 * This method generates the mutated Java files using the major mutation framework
	 * and returns an ordered list of all the mutants for the generateMutantProjects method
	 * to properly create all the projects.  This method also writes the log of all the 
	 * operations done to create each mutant to the mutantLogsPath output directory specified above. 
	 *
	 ***************************************************/
	public static String generateMutantFiles(String projectFolder, String androidSdkFolder, String majorPath, String mutantsFiles, String projectName, String srcFolder, String mutantLogsPath, String masterMutantLog) throws FileNotFoundException, UnsupportedEncodingException{
	
		System.out.println("Getting Android Project Java Files");
		
		StringBuilder mutants = new StringBuilder();  //Declare StringBuilder to build the ordered list of mutant files.
		
		//Get all of the Java files in the src folder of the app source code
		
		String fileList = TerminalHelper.getFilesByNameString(srcFolder,"*.java");
		
		String mutantFileList = "";  //String to hold the properly ordered list of all the mutant files, so that align with the log file
		
		//Set up PrintWriter to print the new log file.
		PrintWriter writer = new PrintWriter(new FileOutputStream(new File(mutantLogsPath + projectName + ".log"),true));		
		
		//Set up BufferedReader to read in all of the paths for the Java Files in the project.
		BufferedReader bufReader = new BufferedReader(new StringReader(fileList));
		
		String line=null;  // Set up variable to hold the line containing the path to the java.
		String m_line =null; //Set up variable to hold the 
		
		System.out.println("Done Getting Java Files!");
		
		System.out.println("Generating Mutant Files... Please Wait");
		
		try {
			//While loop to generate a mutated file for each Java File
			while( (line=bufReader.readLine()) != null )
			{
				if (!line.contains("EmmaInstrumentation.java") && !line.contains("FinishListener.java") && !line.contains("InstrumentedActivity.java") && !line.contains("SMSInstrumentedReceiver.java")){
				int f_counter = 0;
				String javaFile = line.substring(line.lastIndexOf("/")+1, line.lastIndexOf("."));
				//System.out.println(javaFile);
				String command = majorPath + "/bin/javac -XMutator:ALL -J-Dmajor.export.mutants=true -J-Dmajor.export.directory=" + mutantsFiles + File.separator + projectName + File.separator + javaFile + " " + line;
				
				String output = TerminalHelper.executeCommand(command);
				//System.out.println(output);
				//Concatenate LogFiles
				
				
				//Get the list of mutants generated for the java file
				String mutantList = TerminalHelper.getFilesByNameString(mutantsFiles + File.separator + projectName + File.separator + javaFile,"*.java");
				
				//Create BufferedReader for the mutantList to read in each file
				BufferedReader reader = new BufferedReader(new StringReader(mutantList));
				//Create a StringBuilder to build the ordered list of mutants.
				StringBuilder sbuildr = new StringBuilder();
				
				
				//Check to make sure major created mutants for a given java file.
				if (!mutantList.contains("No such file or directory")){ 
				
				//While loop to get the number of mutants generated for the java file
				while( (m_line=reader.readLine()) != null ){
					f_counter ++;
				}
				
				//for loop to process all of the java files in order and append them to the properly ordered list
				for (int i=1 ; i<f_counter+1; i++){
					
					String temp_list = TerminalHelper.getFilesByNameString(mutantsFiles + File.separator + projectName + File.separator + javaFile + File.separator + i,"*.java");
					
					sbuildr.append(temp_list);
					sbuildr.append('\n');
					}
				
				reader.close();  // Close Bufferedreader for list of mutants
				//Append the list of mutants for the java file in question to the ordered list of mutants
				mutants.append(sbuildr.toString());
				
				}//End if statement to check if mutants were generated for a given java file
				
				
				//Read in the master mutant log and write this out to a new log with the name of the project
				BufferedReader br = new BufferedReader(new FileReader(masterMutantLog));
				try {
				    StringBuilder sb = new StringBuilder();
				    String log = br.readLine();

				    while (log != null) {
				        sb.append(log);
				        sb.append("\n");
				        log = br.readLine();
				    }
				    String everything = sb.toString();
					writer.print(everything);
				} finally {
				    br.close();
				}
				// End Concatenate Log Files
			}
			}//End Loop to generate and process mutant files
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// End try/catch
		
		
		
		writer.close();  //Close PrintWriter to write new mutant log file
		
		
		//Count the number of mutants generated.
		String mutantList = TerminalHelper.getFilesByNameString(mutantsFiles + File.separator + projectName,"*.java");
		int mutant_num = countMutantFiles(mutantList);
		
		
		//Print out completion message to user
		System.out.println(mutant_num + " Mutant Files successfully generated.");
		
		
		//assign the properly ordered list of created mutant files to String and return the string.
		mutantFileList = mutants.toString();
		return mutantFileList;
		
	}// End generateMutantFiles Method
	
	/**************************************************
	 * @author KevinMoran
	 *
	 * This method generates the mutated Android projects by taking in the ordered
	 * list of mutated Java files and copying the original projectFolder and replacing
	 * one java file in each copied project with the corresponding mutated version of that file.
	 * The mutated projects are numbered 1 to the number of mutated files generated.  In other
	 * words each mutated java file corresponds to one mutated App project.
	 *
	 ***************************************************/
	public static void generateMutantProjects(String projectFolder, String projectName, String mutantsPath, String mutant_Files){
		
		//BufferedReader to generate the list of mutated java files
		BufferedReader bufReader = new BufferedReader(new StringReader(mutant_Files));
		
		//Progress counter to properly name each mutant project
		int progress_ctr = 1;
		
		//String to hold the read-in line for each mutated java file
		String line=null;
		
		System.out.println("Generating Mutant Projects... Please Wait");
		
		//Create directory to hold mutant projects (If there are a lot of mutants, there needs to sufficent space in the mutantsPath location.
		String command0 = "mkdir -p " + "\"" + mutantsPath + File.separator + projectName + "\""; 
		//System.out.println(command0);
		String output0 = TerminalHelper.executeCommand(command0);
		
		try {
			//While loop to create a mutant project from each mutant java file.
			while( (line=bufReader.readLine()) != null )
			{

				String javaFile = line.substring(line.lastIndexOf("/")+1, line.lastIndexOf("."));
				//System.out.println(javaFile);
				
				String command00 = "mkdir -p " + "\"" + mutantsPath + File.separator + projectName + File.separator + progress_ctr + "\""; 
				//System.out.println("Command: " + command00);
				String output00 = TerminalHelper.executeCommand(command00);
				//System.out.println("Result: " + output00);
				String command = "cp -R " + projectFolder + " " + mutantsPath + File.separator + projectName + File.separator + progress_ctr; 
				//System.out.println(command);
				String output = TerminalHelper.executeCommand(command);
				
				//System.out.println(output);
				
				String file_to_replace = TerminalHelper.getFilesByNameString(mutantsPath + File.separator + projectName  + File.separator + progress_ctr,javaFile+".java");
				
				String command2 = "cp " + line + " " + file_to_replace; 
				String output2 = TerminalHelper.executeCommand(command2);
				
				//System.out.println(output2);
				
				progress_ctr ++;
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Finished Generating " + progress_ctr + " mutants, no errors detected.");
		
	}
	
	
	/**************************************************
	 * @author KevinMoran
	 *
	 * Method to count the number of mutant files created
	 *
	 ***************************************************/
	public static int countMutantFiles(String mutantList){
		
		int mutant_ctr = 0;
		
		BufferedReader bufReader = new BufferedReader(new StringReader(mutantList));
		
		String line=null;
		
		try {
			while( (line=bufReader.readLine()) != null )
			{

				mutant_ctr++;
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mutant_ctr;
	}
	
	
	/**************************************************
	 * @author KevinMoran
	 *
	 * Method to properly order the entries in the log files to correspond to the names of the 
	 * mutated projects
	 *
	 ***************************************************/
	public static void FixLogFiles(String projectName, String mutantLogsPath) throws IOException{
		
		PrintWriter writer = new PrintWriter(new FileOutputStream(new File(mutantLogsPath + projectName + "-fixed.log"),true));	
		
		int mutant_ctr =1;
		
		System.out.println("Fixing Mutant Log File for " + projectName + "...");
		
		BufferedReader br = new BufferedReader(new FileReader(new File(mutantLogsPath + projectName + ".log"))); 
		    String line;
		    while ((line = br.readLine()) != null) {
		       
		    	line = line.substring(line.indexOf(":"), line.length());
		    	//System.out.println(line);
		    	writer.println(mutant_ctr + line);
		    	
		    	mutant_ctr++;
		    }
		    writer.close();
		    br.close();
		    
		
		
		System.out.println("Done fixing log file!");
		
	}
	
}

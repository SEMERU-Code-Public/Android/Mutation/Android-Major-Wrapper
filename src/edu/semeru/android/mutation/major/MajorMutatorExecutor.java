/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies,
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/
/**

 * Created by Kevin Moran on Mar 24, 2016
 */
package edu.semeru.android.mutation.major;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author KevinMoran
 *
 */
public class MajorMutatorExecutor {

	public static void main(String[] args) throws IOException {
	
		String projectFileList = args[0];
		List<String> projectList = new ArrayList<String>();
		String androidSdkFolder = args[1];
		String majorPath = args[2];
		String mutantsFiles = args[3];
		String mutantsPath = args[4];
		String projectName ="";
		String mutantLogsPath = args[5]; //This should be a directory 
		String tempMutantLog = args[6]; //This should be a .log file
		String srcFolder ="";
		
		
		
		try (BufferedReader br = new BufferedReader(new FileReader(projectFileList))) {
			String line;
			while ((line = br.readLine()) != null) {
				projectList.add(line);
			} // End While loop to store list of Files in an array
		} // End Try Catch to read in the list of files 

		for (String currentFile : projectList){
			
			projectName = currentFile.substring(currentFile.lastIndexOf("/")+1,currentFile.length());
			currentFile = currentFile + File.separator;
			System.out.println("Starting Mutation for the following Project: " + projectName);
			srcFolder = currentFile + File.separator + "src/";
			System.out.println("Project Folder: " + currentFile);
			
			System.out.println("Generating Mutant Files for " + projectName + " in the following directory:");
			System.out.println(mutantsFiles + projectName + File.separator);
			String mutant_Files = MajorMutationWrapper.generateMutantFiles(currentFile, androidSdkFolder, majorPath, mutantsFiles, projectName, srcFolder, mutantLogsPath, tempMutantLog);
			MajorMutationWrapper.FixLogFiles(projectName, mutantLogsPath);
			MajorMutationWrapper.generateMutantProjects(currentFile, projectName, mutantsPath, mutant_Files);
			
		}//End for loop to generate mutants
		
		
		
	}// End main method
	
}
